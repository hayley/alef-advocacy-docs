# Building your first EdgeNet app

Welcome to the edge! In this tutorial, we will build an edge-enabled video streaming app with the Alef Edgenet platform by utilising their Video Enablement API. 

Please subscribe to the Getting Started challenge for AlefEdge beforehand. It is assumed that you have completed this challenge and have a developer account for the AlefEdge portal.

https://www.topcoder.com/challenges/56cee82e-28ee-4ae0-8b14-1154086ba8cb

## Introduction

Displaying media on web sites is a standard task nowadays. When streaming content, the media files usually reside on a CDN server, from where they are fetched to the browser by front-end logic. 

Thus, the main speed bottle-neck is caused by the connection between the media CDN and the client. The Alef EdgeNet platform brings a solution to this problem by providing an edge computing network, where content is automatically loaded to the closest Edge location to the client, much closer than a traditional data center. Additionally, the Edge infrastructure is able to leverage 5G technology behind the scenes, even if the client device is not 5G-enabled.

## Alef Video Enablement API

After creating your service subscription on the Alef development portal, you are able to start using the API.

Let's use the following video for our application: https://dummypartner.testalef.net/alef/openingshot.mp4 We will first import this video to our service through the API. The interactive API documentation at https://developerapis.dev-alefedge.com/api-docs-edgetube/ allows us to call the API without the need for a terminal.

```
curl -X 'POST' \
  'https://developerapis.dev-alefedge.com/et/api/v1/stream-tech/content/add?partner_name=YOUR_SERVICE_NAME' \
  -H 'accept: application/json' \
  -H 'api_key: YOUR-API-KEY' \
  -H 'Content-Type: application/json' \
  -d '{
  "urlList": [
    {
      "url": "https://dummypartner.testalef.net/alef/openingshot.mp4",
      "content_access": "public",
      "publish_access": "public",
      "partner_cloud_url": "https://dummypartner.testalef.net/alef/openingshot.mp4"
    }
  ]
}'
```


After this, we may check the status by the following API call.

```
curl -X 'GET' \
  'https://developerapis.stg-alefedge.com/et/api/v1/stream-tech/content/get-all?partner_name=YOUR_SERVICE_NAME' \
  -H 'accept: application/json' \
  -H 'api_key: YOUR-API-KEY'
```

```
  {...
  ,
    "id": 1498,
    "partner_name": "YOUR_SERVICE_NAME",
    "content_id": "ahq",
    "content_url": "https://edge.stg-alefedge.com/v1/content?url=00c104e612cb7f461e6446d77125a0edb3b8a827df1bd1149f3a08cad05d4af7252d2990f8dd212ccc1ae00da5b8694c96f774c3e8b8009ceee54033b0b40219",
    "partner_content_url": "https://dummypartner.testalef.net/alef/openingshot.mp4",
    "partner_cloud_url": "https://dummypartner.testalef.net/alef",
    "createdAt": "2021-05-14T22:10:24.000Z",
    "updatedAt": "2021-05-14T22:10:25.000Z",
    "content_upload_status": "success",
    "content_access": "public",
    "publish_access": "public",
    "content_status_message": "Partner Content on-boarded Successfully",
    "partner_content_file_name": "main.mp4",
    "content_size": 1645694
  }, ...
```

Great! Our video has been imported successfully, and we can now view our video from an edge location. Let's request a sandbox to try this out.

```
GET https://edge.stg-alefedge.com/v1/content?url=00c104e612cb7f461e6446d77125a0edb3b8a827df1bd1149f3a08cad05d4af7252d2990f8dd212ccc1ae00da5b8694c96f774c3e8b8009ceee54033b0b40219&api_key=YOUR-API-KEY"
```

![sandbox video](https://i.imgur.com/UshYHnZ.png)

From our sandbox, we are able to view the video successfully. Let's now build a web app to play videos for us.


## Video player web app

We have now successfully tested the Video Enablement API and verified that it works as it should. Let's start building an app to take advantage of this API.

We will be using Heroku as our host, but the same principles hold for any other provider as well. Let's start by creating a started application on Heroku.

https://devcenter.heroku.com/articles/getting-started-with-python#introduction

We follow the above tutorial by Heroku to deploy a Python starter application on Heroku.

```
$ heroku login

$ git clone https://github.com/heroku/python-getting-started.git
```
This will create a starter application with the following folder structure for you:

```
$ ls -lR
.:
total 17
-rw-r--r-- 1 pena 197121  631 kesä   28 14:07 app.json
drwxr-xr-x 1 pena 197121    0 kesä   28 14:07 gettingstarted/
drwxr-xr-x 1 pena 197121    0 kesä   28 14:07 hello/
-rwxr-xr-x 1 pena 197121  267 kesä   28 14:07 manage.py*
-rw-r--r-- 1 pena 197121   35 kesä   28 14:07 Procfile
-rw-r--r-- 1 pena 197121   46 kesä   28 14:07 Procfile.windows
-rw-r--r-- 1 pena 197121 1374 kesä   28 14:07 README.md
-rw-r--r-- 1 pena 197121   31 kesä   28 14:07 requirements.txt
-rw-r--r-- 1 pena 197121   14 kesä   28 14:07 runtime.txt

./gettingstarted:
total 6
-rw-r--r-- 1 pena 197121    0 kesä   28 14:07 __init__.py
-rw-r--r-- 1 pena 197121 3338 kesä   28 14:07 settings.py
drwxr-xr-x 1 pena 197121    0 kesä   28 14:07 static/
-rw-r--r-- 1 pena 197121  484 kesä   28 14:07 urls.py
-rw-r--r-- 1 pena 197121  421 kesä   28 14:07 wsgi.py

./gettingstarted/static:
total 0
-rw-r--r-- 1 pena 197121 0 kesä   28 14:07 humans.txt

./hello:
total 8
-rw-r--r-- 1 pena 197121   0 kesä   28 14:07 __init__.py
-rw-r--r-- 1 pena 197121  66 kesä   28 14:07 admin.py
drwxr-xr-x 1 pena 197121   0 kesä   28 14:07 migrations/
-rw-r--r-- 1 pena 197121 159 kesä   28 14:07 models.py
drwxr-xr-x 1 pena 197121   0 kesä   28 14:07 static/
drwxr-xr-x 1 pena 197121   0 heinä   5 15:12 templates/
-rw-r--r-- 1 pena 197121 613 kesä   28 14:07 tests.py
-rw-r--r-- 1 pena 197121 429 kesä   28 14:07 views.py

./hello/migrations:
total 4
-rw-r--r-- 1 pena 197121   0 kesä   28 14:07 __init__.py
-rw-r--r-- 1 pena 197121 612 kesä   28 14:07 0001_initial.py

./hello/static:
total 4
-rw-r--r-- 1 pena 197121 2217 kesä   28 14:07 lang-logo.png

./hello/templates:
total 17
-rw-r--r-- 1 pena 197121 3602 kesä   28 14:07 base.html
-rw-r--r-- 1 pena 197121  257 kesä   28 14:07 db.html
-rw-r--r-- 1 pena 197121 1967 kesä   28 15:41 index.html

```

Let's use this starter application as our basis. We can remove the unnecessary parts later.

```
$ cd python-getting-started

$ heroku create

$ git push heroku main

$ heroku ps:scale web=1

```
You may call
```
$ heroku open
```

to view your app.

We can now remove some boilerplate and modify the hello/templates/index.html page to build the first version of our app.

To begin with, let's make a quick and simple comparison site where we present the same video from the EdgeNet and from the original source side by side. We can add some quick styling to make it look nice.


```
.mp4 file <br/>
left is from cloud, right is from the edge API
<br/>

<embed src="https://dummypartner.testalef.net/alef/openingshot.mp4" autostart="false" height="600" width="800" /></embed>

<embed src="https://edge.stg-alefedge.com/v1/content?url=00c104e612cb7f461e6446d77125a0edb3b8a827df1bd1149f3a08cad05d4af7252d2990f8dd212ccc1ae00da5b8694c96f774c3e8b8009ceee54033b0b40219&api_key=YOUR-API-KEY" autostart="false" height="600" width="800" /></embed>
```

Now, let's push our changes to Heroku and go view our app!

![image](https://i.imgur.com/VijH9Yx.png)



Success! The edge version is faster as well! Now, let's consider another challenge: streaming media files in the .m3u8 format. These files allow media files to be streamed part by part into the browser as opposed to whole .mp4 files. The Video Enablement API supports both types of media.

## Streaming media

Let's use the following example file for our app.

https://dummypartner.testalef.net/demo/myencoding.m3u8

We start by importing this media into our service through the API.


```
curl -X 'POST' \
  'https://developerapis.stg-alefedge.com/et/api/v1/stream-tech/content/add?partner_name=YOUR_SERVICE_NAME' \
  -H 'accept: application/json' \
  -H 'api_key: YOUR-API-KEY' \
  -H 'Content-Type: application/json' \
  -d '{
  "urlList": [
    {
      "url": "https://dummypartner.testalef.net/demo/myencoding.m3u8",
      "content_access": "public",
      "publish_access": "public",
      "partner_cloud_url": "https://dummypartner.testalef.net/demo/myencoding.m3u8"
    }
  ]
}'
```

Now, we must include our previously created web app as a whitelisted URL in our service, we may do this through the API.

```
curl -X 'POST' \
  'https://developerapis.stg-alefedge.com/et/api/v1/stream-tech/add-whitelist-domain?partner_name=YOUR_SERVICE_NAME' \
  -H 'accept: application/json' \
  -H 'api_key: YOUR-API-KEY' \
  -H 'Content-Type: application/json' \
  -d '{
  "cors_whitelist_domain": "https://evening-chamber-32717.herokuapp.com"
}'
```

```
{
  "status": "SUCCESS",
  "message": "Cors whitelist domain added successfully."
}
```

We must modify our previously creted web app to support this new media type. Let's create a new page for this, with modified contents from our previous page.

```
video.js version

<!-- unpkg : use the latest version of Video.js -->
<link href="https://unpkg.com/video.js/dist/video-js.min.css" rel="stylesheet">
<script src="https://unpkg.com/video.js/dist/video.min.js"></script>

<video
    id="my-player"
    class="video-js"
    controls
    preload="auto"
    poster="//vjs.zencdn.net/v/oceans.png"
    data-setup='{}'>
  <source src="https://dummypartner.testalef.net/demo/myencoding.m3u8" type='application/x-mpegURL'></source>
  <p class="vjs-no-js">
    To view this video please enable JavaScript, and consider upgrading to a
    web browser that
    <a href="https://videojs.com/html5-video-support/" target="_blank">
      supports HTML5 video
    </a>
  </p>
</video>

<video
    id="player2"
    class="video-js"
    controls
    preload="auto"
    poster="https://upload.wikimedia.org/wikipedia/commons/4/47/PNG_transparency_demonstration_1.png"
    data-setup='{}'>
  <source src="https://edge.stg-alefedge.com/v1/content?url=00c104e612cb7f461e6446d77125a0edb3b8a827df1bd1149f3a08cad05d4af706df9bfcc15423bf0c23351f723e73be1dc896eb2aa7dbfaddfcedb6a230b8c4&api_key=YOUR-API-KEY" type='application/x-mpegURL'></source>
  <p class="vjs-no-js">
    To view this video please enable JavaScript, and consider upgrading to a
    web browser that
    <a href="https://videojs.com/html5-video-support/" target="_blank">
      supports HTML5 video
    </a>
  </p>
</video>
```

The Video.js library allows for playing of .m3u8 files. Other libraries exist as well. A player library like this is needed because HLS media (.m3u8) is not natively supported in HTML5. We can use the latest release from the CDN for now, but for production use we might want to choose a set version and serve it ourselves so as not to be dependent on CDN availability.


![image .m3u8](https://i.imgur.com/yCS9Na6.png)

Yay! Great success! Our code now works and we are able to play HLS media in our web app through the Video Enablement API. We also note the difference in load times between the edge domain and the original cloud source from the browser developer tools.

## Next steps

After our technical parts are working, we can do some cleaning and additional development for our app. The app we've built so far may be good enough for our testing purposes, but it's not something you'd want to show anyone else, let alone put into production, right? Let's make the UI nicer and encapsulate our logic with React(?) 

```
CODE
```


## References & further reading

Topcoder Edgenet community program https://www.topcoder.com/community/member-programs/edgenet?tracks[edgenet-compete]=1

Alef https://alefedge.com/

Alef EdgeNet https://alefedge.com/alef-edgenet/

Alef Video Enablement API https://alefedge.com/products/prepackaged-solutions/alef-video-enablement-1/

Wikipedia, Edge computing, https://en.wikipedia.org/wiki/Edge_computing

